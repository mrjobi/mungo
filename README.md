## Mungo.

Chef interpreter (and transpiler to python) written in python.

Chef is an esoteric programming language by David Morgan-Mar - http://www.dangermouse.net/esoteric/chef.html

Mungo is the name of the Cook in Monty Python's "Restaurant Sketch" (a.k.a Dirty Fork)

(c) 2014 marek.job, licensed under WTFPL http://www.wtfpl.net/