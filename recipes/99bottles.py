from collections import defaultdict
from random import shuffle

class Ing :
	def __init__(self,name,value,state) :
		self.name=name
		self.value=value
		self.state=state
	def __add__(self,other):
		return Ing(self.name,self.value+other.value,self.state)
	def __sub__(self,other):
		return Ing(self.name,self.value-other.value,self.state)
	def __mul__(self,other):
		return Ing(self.name,self.value*other.value,self.state)
	def __div__(self,other):
		return Ing(self.name,self.value/other.value,self.state)
	def __isub__(self,val) :
		return Ing(self.name,self.value-val,self.state)
	def __nonzero__(self) :
		return self.value!=0
	def __str__(self) :
		v=self.value
		return "%c"%v if self.state==False else " %d"%v
	def __repr__(self) :
		return str(self)
	def liquefy(self) :
		self.state=False
	def copy(self) :
		return Ing(self.name,self.value,self.state)

def stir(stack,num) :
	n=int(num)
	depth=n if len(stack)>=n else len(stack)
	i=stack.pop()
	stack.insert(-depth,i)

def copydata(data) :
	bowls,dishes=data
	
	s=defaultdict(list)
	for k,v in bowls.items() :
		s[k]=[x.copy() for x in v]
	d=defaultdict(list)
	for k,v in bowls.items() :
		d[k]=[x.copy() for x in v]
	return s,d

def printdata(dishes,count) :
	result=[]
	for i in range(1,count+1) :
		d=dishes[i]
		while d :
			result.append(str(d.pop()))
	print "".join(result)

def hops(data=(defaultdict(list),defaultdict(list))) :
	s,d=copydata(data)
	Tettnanger=Ing('Tettnanger',111,False)
	Saaz=Ing('Saaz',32,False)
	Hersbruck=Ing('Hersbruck',104,False)
	Golding=Ing('Golding',110,False)
	Spalter=Ing('Spalter',116,False)
	sugar=Ing('sugar',97,False)
	water=Ing('water',10,False)
	Hallertauer_Mittelfrueh=Ing('Hallertauer_Mittelfrueh',108,False)
	Fuggle=Ing('Fuggle',101,False)
	Styrian=Ing('Styrian',119,False)
	ingredients=[Tettnanger, Saaz, Hersbruck, Golding, Spalter, sugar, water, Hallertauer_Mittelfrueh, Fuggle, Styrian]

	s[1]=[] #Clean 1st mixing bowl.
	s[1].append(water.copy()) #Put water into the 1st mixing bowl.
	s[1].append(Hallertauer_Mittelfrueh.copy()) #Put Hallertauer Mittelfrueh into the 1st mixing bowl.
	s[1].append(Hallertauer_Mittelfrueh.copy()) #Put Hallertauer Mittelfrueh into the 1st mixing bowl.
	s[1].append(sugar.copy()) #Put sugar into the 1st mixing bowl.
	s[1].append(Styrian.copy()) #Put Styrian into the 1st mixing bowl.
	s[1].append(Saaz.copy()) #Put Saaz into the 1st mixing bowl.
	s[1].append(Fuggle.copy()) #Put Fuggle into the 1st mixing bowl.
	s[1].append(Hersbruck.copy()) #Put Hersbruck into the 1st mixing bowl.
	s[1].append(Spalter.copy()) #Put Spalter into the 1st mixing bowl.
	s[1].append(Saaz.copy()) #Put Saaz into the 1st mixing bowl.
	s[1].append(Golding.copy()) #Put Golding into the 1st mixing bowl.
	s[1].append(Tettnanger.copy()) #Put Tettnanger into the 1st mixing bowl.
	s[1].append(Saaz.copy()) #Put Saaz into the 1st mixing bowl.
	return s[1]
	


def _99_bottles_of_beer_brewed_on_malted_barley_hops_and_assorted_adjuncts(data=(defaultdict(list),defaultdict(list))) :
	s,d=copydata(data)
	wild_yeasts=Ing('wild_yeasts',32,False)
	mixture=Ing('mixture',1,None)
	sugar=Ing('sugar',1,True)
	water=Ing('water',10,False)
	ale_yeast=Ing('ale_yeast',111,False)
	barley=Ing('barley',3,True)
	lager_yeast=Ing('lager_yeast',78,False)
	malt_extract=Ing('malt_extract',0,True)
	ingredients=[wild_yeasts, mixture, sugar, water, ale_yeast, barley, lager_yeast, malt_extract]

	s[2].append(malt_extract.copy()) #Put malt extract into the 2nd mixing bowl.
	while barley :
		cannot_make_totally_empty_loop_in_python='noop' #Mash the barley.
		s[1].append(water.copy()) #Put water into the 1st mixing bowl.
		s[1].extend(hops((s,d))) #Serve with hops.
		s[1].extend(malted_barley((s,d))) #Serve with malted barley.
		while malt_extract :
			cannot_make_totally_empty_loop_in_python='noop' #Boil the malt extract.
			s[1].append(malt_extract.copy()) #Put malt extract into the 1st mixing bowl.
			break #Set aside. #Stir until boiled.
		while mixture :
			cannot_make_totally_empty_loop_in_python='noop' #Ferment the mixture.
			s[1].append(ale_yeast.copy()) #Put ale yeast into the 1st mixing bowl.
			s[1].append(lager_yeast.copy()) #Put lager yeast into the 1st mixing bowl.
			s[1].append(wild_yeasts.copy()) #Put wild yeasts into the 1st mixing bowl.
			mixture-=1 #Condition the mixture until fermented.
		s[2].append(s[2][-1]+sugar) #Add sugar into the 2nd mixing bowl.
		malt_extract=s[2].pop() #Fold malt extract into the 2nd mixing bowl.
		s[1].extend(adjuncts((s,d))) #Serve with adjuncts.
		s[2].append(malt_extract.copy()) #Put malt extract into the 2nd mixing bowl.
		s[1].extend(malted_barley((s,d))) #Serve with malted barley.
		s[1].append(malt_extract.copy()) #Put malt extract into the 1st mixing bowl.
		s[1].extend(hops((s,d))) #Serve with hops.
		s[1].extend(malted_barley((s,d))) #Serve with malted barley.
		s[1].append(malt_extract.copy()) #Put malt extract into the 1st mixing bowl.
		barley-=1 #Mash the barley until mashed.
	d[1].extend([x.copy() for x in s[1]]) #Pour contents of the 1st mixing bowl into the baking dish.
	printdata(d,1) #Serves 1
	return s[1]
	


def malted_barley(data=(defaultdict(list),defaultdict(list))) :
	s,d=copydata(data)
	victory_malt=Ing('victory_malt',101,False)
	Dextrin_malt=Ing('Dextrin_malt',114,False)
	lager_malt=Ing('lager_malt',98,False)
	Munich_malt=Ing('Munich_malt',115,False)
	rye_malt=Ing('rye_malt',108,False)
	water=Ing('water',32,False)
	pale_ale_malt=Ing('pale_ale_malt',111,False)
	barley=Ing('barley',1,True)
	Vienna_malt=Ing('Vienna_malt',102,False)
	wheat_malt=Ing('wheat_malt',116,False)
	ingredients=[victory_malt, Dextrin_malt, lager_malt, Munich_malt, rye_malt, water, pale_ale_malt, barley, Vienna_malt, wheat_malt]

	s[2].append(s[2][-1]-barley) #Remove barley from the 2nd mixing bowl.
	barley=s[2].pop() #Fold barley into the 2nd mixing bowl.
	s[1]=[] #Clean 1st mixing bowl.
	s[1].append(Dextrin_malt.copy()) #Put Dextrin malt into the 1st mixing bowl.
	s[1].append(victory_malt.copy()) #Put victory malt into the 1st mixing bowl.
	s[1].append(victory_malt.copy()) #Put victory malt into the 1st mixing bowl.
	s[1].append(lager_malt.copy()) #Put lager malt into the 1st mixing bowl.
	s[1].append(water.copy()) #Put water into the 1st mixing bowl.
	s[1].append(Vienna_malt.copy()) #Put Vienna malt into the 1st mixing bowl.
	s[1].append(pale_ale_malt.copy()) #Put pale ale malt into the 1st mixing bowl.
	s[1].append(water.copy()) #Put water into the 1st mixing bowl.
	while barley :
		cannot_make_totally_empty_loop_in_python='noop' #Kiln the barley.
		s[1].append(Munich_malt.copy()) #Put Munich malt into the 1st mixing bowl.
		break #Set aside.
		barley-=1 #Dry the barley until kilned.
	s[1].append(victory_malt.copy()) #Put victory malt into the 1st mixing bowl.
	s[1].append(rye_malt.copy()) #Put rye malt into the 1st mixing bowl.
	s[1].append(wheat_malt.copy()) #Put wheat malt into the 1st mixing bowl.
	s[1].append(wheat_malt.copy()) #Put wheat malt into the 1st mixing bowl.
	s[1].append(pale_ale_malt.copy()) #Put pale ale malt into the 1st mixing bowl.
	s[1].append(lager_malt.copy()) #Put lager malt into the 1st mixing bowl.
	s[1].append(water.copy()) #Put water into the 1st mixing bowl.
	return s[1]
	


def adjuncts(data=(defaultdict(list),defaultdict(list))) :
	s,d=copydata(data)
	honey=Ing('honey',32,False)
	smashed_bananas=Ing('smashed_bananas',112,False)
	wheat=Ing('wheat',105,False)
	oats=Ing('oats',115,False)
	corn=Ing('corn',116,False)
	rye=Ing('rye',117,False)
	black_berries=Ing('black_berries',110,False)
	water=Ing('water',10,False)
	grape_juice=Ing('grape_juice',119,False)
	mango=Ing('mango',84,False)
	wild_berries=Ing('wild_berries',100,False)
	brown_sugar=Ing('brown_sugar',111,False)
	prune_extract=Ing('prune_extract',97,False)
	red_currants=Ing('red_currants',107,False)
	rice=Ing('rice',101,False)
	apple_juice=Ing('apple_juice',114,False)
	ingredients=[honey, smashed_bananas, wheat, oats, corn, rye, black_berries, water, grape_juice, mango, wild_berries, brown_sugar, prune_extract, red_currants, rice, apple_juice]

	s[1]=[] #Clean 1st mixing bowl.
	s[1].append(water.copy()) #Put water into the 1st mixing bowl.
	s[1].append(wild_berries.copy()) #Put wild berries into the 1st mixing bowl.
	s[1].append(black_berries.copy()) #Put black berries into the 1st mixing bowl.
	s[1].append(rye.copy()) #Put rye into the 1st mixing bowl.
	s[1].append(brown_sugar.copy()) #Put brown sugar into the 1st mixing bowl.
	s[1].append(apple_juice.copy()) #Put apple juice into the 1st mixing bowl.
	s[1].append(prune_extract.copy()) #Put prune extract into the 1st mixing bowl.
	s[1].append(honey.copy()) #Put honey into the 1st mixing bowl.
	s[1].append(corn.copy()) #Put corn into the 1st mixing bowl.
	s[1].append(wheat.copy()) #Put wheat into the 1st mixing bowl.
	s[1].append(honey.copy()) #Put honey into the 1st mixing bowl.
	s[1].append(oats.copy()) #Put oats into the 1st mixing bowl.
	s[1].append(oats.copy()) #Put oats into the 1st mixing bowl.
	s[1].append(prune_extract.copy()) #Put prune extract into the 1st mixing bowl.
	s[1].append(smashed_bananas.copy()) #Put smashed bananas into the 1st mixing bowl.
	s[1].append(honey.copy()) #Put honey into the 1st mixing bowl.
	s[1].append(wild_berries.copy()) #Put wild berries into the 1st mixing bowl.
	s[1].append(black_berries.copy()) #Put black berries into the 1st mixing bowl.
	s[1].append(prune_extract.copy()) #Put prune extract into the 1st mixing bowl.
	s[1].append(honey.copy()) #Put honey into the 1st mixing bowl.
	s[1].append(black_berries.copy()) #Put black berries into the 1st mixing bowl.
	s[1].append(grape_juice.copy()) #Put grape juice into the 1st mixing bowl.
	s[1].append(brown_sugar.copy()) #Put brown sugar into the 1st mixing bowl.
	s[1].append(wild_berries.copy()) #Put wild berries into the 1st mixing bowl.
	s[1].append(honey.copy()) #Put honey into the 1st mixing bowl.
	s[1].append(rice.copy()) #Put rice into the 1st mixing bowl.
	s[1].append(black_berries.copy()) #Put black berries into the 1st mixing bowl.
	s[1].append(brown_sugar.copy()) #Put brown sugar into the 1st mixing bowl.
	s[1].append(honey.copy()) #Put honey into the 1st mixing bowl.
	s[1].append(rice.copy()) #Put rice into the 1st mixing bowl.
	s[1].append(red_currants.copy()) #Put red currants into the 1st mixing bowl.
	s[1].append(prune_extract.copy()) #Put prune extract into the 1st mixing bowl.
	s[1].append(mango.copy()) #Put mango into the 1st mixing bowl.
	s[1].append(honey.copy()) #Put honey into the 1st mixing bowl.
	s[1].append(water.copy()) #Put water into the 1st mixing bowl.
	return s[1]
	



_99_bottles_of_beer_brewed_on_malted_barley_hops_and_assorted_adjuncts()