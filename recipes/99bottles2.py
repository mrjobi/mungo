from collections import defaultdict
from random import shuffle

class Ing :
	def __init__(self,name,value,state) :
		self.name=name
		self.value=value
		self.state=state
	def __add__(self,other):
		return Ing(self.name,self.value+other.value,self.state)
	def __sub__(self,other):
		return Ing(self.name,self.value-other.value,self.state)
	def __mul__(self,other):
		return Ing(self.name,self.value*other.value,self.state)
	def __div__(self,other):
		return Ing(self.name,self.value/other.value,self.state)
	def __isub__(self,val) :
		return Ing(self.name,self.value-val,self.state)
	def __nonzero__(self) :
		return self.value!=0
	def __str__(self) :
		v=self.value
		return "%c"%v if self.state==False else " %d"%v
	def __repr__(self) :
		return str(self)
	def liquefy(self) :
		self.state=False
	def copy(self) :
		return Ing(self.name,self.value,self.state)

def stir(stack,num) :
	n=int(num)
	depth=n if len(stack)>=n else len(stack)
	i=stack.pop()
	stack.insert(-depth,i)

def copydata(data) :
	bowls,dishes=data
	
	s=defaultdict(list)
	for k,v in bowls.items() :
		s[k]=[x.copy() for x in v]
	d=defaultdict(list)
	for k,v in bowls.items() :
		d[k]=[x.copy() for x in v]
	return s,d

def printdata(dishes,count) :
	result=[]
	for i in range(1,count+1) :
		d=dishes[i]
		while d :
			result.append(str(d.pop()))
	print "".join(result)

def _99_bottles_of_beer(data=(defaultdict(list),defaultdict(list))) :
	s,d=copydata(data)
	bottles=Ing('bottles',99,None)
	ingredients=[bottles]

	while bottles :
		cannot_make_totally_empty_loop_in_python='noop' #Loop the bottles.
		s[1].append(bottles.copy()) #Put bottles into 1st mixing bowl.
		s[1].extend(bottles_of_beer_on_the_wall((s,d))) #Serve with bottles of beer on the wall.
		s[1]=[] #Clean 1st mixing bowl.
		s[1].append(bottles.copy()) #Put bottles into 1st mixing bowl.
		s[1].extend(bottles_of_beer((s,d))) #Serve with bottles of beer.
		s[1]=[] #Clean 1st mixing bowl.
		s[1].extend(take_one_down_and_pass_it_around((s,d))) #Serve with Take one down and pass it around.
		s[1]=[] #Clean 1st mixing bowl.
		bottles-=1 #Loop the bottles until looped.
	s[1].extend(no_more_bottles_of_beer((s,d))) #Serve with No more bottles of beer.
	s[1]=[] #Clean 1st mixing bowl.
	d[1].extend([x.copy() for x in s[3]]) #Pour contents of the 3rd mixing bowl into the 1st baking dish.
	printdata(d,1) #Serves 1
	return s[1]
	


def bottles_of_beer_on_the_wall(data=(defaultdict(list),defaultdict(list))) :
	s,d=copydata(data)
	raspberry=Ing('raspberry',114,None)
	huckleberry=Ing('huckleberry',104,True)
	onion=Ing('onion',111,None)
	asparagus=Ing('asparagus',97,None)
	nannyberry=Ing('nannyberry',110,None)
	squach=Ing('squach',115,None)
	new_line=Ing('new_line',10,False)
	feijoa=Ing('feijoa',102,True)
	turnip=Ing('turnip',116,None)
	eggplant=Ing('eggplant',101,True)
	pickles=Ing('pickles',32,None)
	watercress=Ing('watercress',119,True)
	broccoli=Ing('broccoli',98,True)
	lime=Ing('lime',108,True)
	ingredients=[raspberry, huckleberry, onion, asparagus, nannyberry, squach, new_line, feijoa, turnip, eggplant, pickles, watercress, broccoli, lime]

	s[1].append(new_line.copy()) #Put new line into 1st mixing bowl.
	s[2].append(lime.copy()) #Put lime into 2nd mixing bowl.
	s[2].append(lime.copy()) #Put lime into 2nd mixing bowl.
	s[2].append(asparagus.copy()) #Put asparagus into 2nd mixing bowl.
	s[2].append(watercress.copy()) #Put watercress into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	s[2].append(eggplant.copy()) #Put eggplant into 2nd mixing bowl.
	s[2].append(huckleberry.copy()) #Put huckleberry into 2nd mixing bowl.
	s[2].append(turnip.copy()) #Put turnip into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	s[2].append(nannyberry.copy()) #Put nannyberry into 2nd mixing bowl.
	s[2].append(onion.copy()) #Put onion into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	s[2].append(raspberry.copy()) #Put raspberry into 2nd mixing bowl.
	s[2].append(eggplant.copy()) #Put eggplant into 2nd mixing bowl.
	s[2].append(eggplant.copy()) #Put eggplant into 2nd mixing bowl.
	s[2].append(broccoli.copy()) #Put broccoli into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	s[2].append(feijoa.copy()) #Put feijoa into 2nd mixing bowl.
	s[2].append(onion.copy()) #Put onion into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	s[2].append(squach.copy()) #Put squach into 2nd mixing bowl.
	s[2].append(eggplant.copy()) #Put eggplant into 2nd mixing bowl.
	s[2].append(lime.copy()) #Put lime into 2nd mixing bowl.
	s[2].append(turnip.copy()) #Put turnip into 2nd mixing bowl.
	s[2].append(turnip.copy()) #Put turnip into 2nd mixing bowl.
	s[2].append(onion.copy()) #Put onion into 2nd mixing bowl.
	s[2].append(broccoli.copy()) #Put broccoli into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	map(lambda x: x.liquefy(), s[2]) #Liquefy contents of the 2nd mixing bowl.
	d[1].extend([x.copy() for x in s[2]]) #Pour contents of the 2nd mixing bowl into the baking dish.
	d[1].extend([x.copy() for x in s[1]]) #Pour contents of the mixing bowl into the baking dish.
	printdata(d,1)
	return s[1] #Refrigerate for 1 hour.
	return s[1]
	


def bottles_of_beer(data=(defaultdict(list),defaultdict(list))) :
	s,d=copydata(data)
	onion=Ing('onion',111,None)
	feijoa=Ing('feijoa',102,None)
	squach=Ing('squach',115,None)
	new_line=Ing('new_line',10,False)
	turnip=Ing('turnip',116,None)
	eggplant=Ing('eggplant',101,True)
	pickles=Ing('pickles',32,True)
	raspberry=Ing('raspberry',114,None)
	broccoli=Ing('broccoli',98,None)
	lime=Ing('lime',108,None)
	ingredients=[onion, feijoa, squach, new_line, turnip, eggplant, pickles, raspberry, broccoli, lime]

	s[1].append(new_line.copy()) #Put new line into 1st mixing bowl.
	s[2].append(raspberry.copy()) #Put raspberry into 2nd mixing bowl.
	s[2].append(eggplant.copy()) #Put eggplant into 2nd mixing bowl.
	s[2].append(eggplant.copy()) #Put eggplant into 2nd mixing bowl.
	s[2].append(broccoli.copy()) #Put broccoli into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	s[2].append(feijoa.copy()) #Put feijoa into 2nd mixing bowl.
	s[2].append(onion.copy()) #Put onion into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	s[2].append(squach.copy()) #Put squach into 2nd mixing bowl.
	s[2].append(eggplant.copy()) #Put eggplant into 2nd mixing bowl.
	s[2].append(lime.copy()) #Put lime into 2nd mixing bowl.
	s[2].append(turnip.copy()) #Put turnip into 2nd mixing bowl.
	s[2].append(turnip.copy()) #Put turnip into 2nd mixing bowl.
	s[2].append(onion.copy()) #Put onion into 2nd mixing bowl.
	s[2].append(broccoli.copy()) #Put broccoli into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	map(lambda x: x.liquefy(), s[2]) #Liquefy contents of the 2nd mixing bowl.
	d[1].extend([x.copy() for x in s[2]]) #Pour contents of the 2nd mixing bowl into the baking dish.
	d[1].extend([x.copy() for x in s[1]]) #Pour contents of the mixing bowl into the baking dish.
	printdata(d,1)
	return s[1] #Refrigerate for 1 hour.
	return s[1]
	


def no_more_bottles_of_beer(data=(defaultdict(list),defaultdict(list))) :
	s,d=copydata(data)
	new_line=Ing('new_line',10,False)
	nectarine=Ing('nectarine',78,True)
	onion=Ing('onion',111,None)
	feijoa=Ing('feijoa',102,True)
	squach=Ing('squach',115,None)
	mushrooms=Ing('mushrooms',109,None)
	turnip=Ing('turnip',116,True)
	eggplant=Ing('eggplant',101,None)
	pickles=Ing('pickles',32,None)
	raspberry=Ing('raspberry',114,True)
	broccoli=Ing('broccoli',98,None)
	lime=Ing('lime',108,None)
	ingredients=[new_line, nectarine, onion, feijoa, squach, mushrooms, turnip, eggplant, pickles, raspberry, broccoli, lime]

	s[3].append(new_line.copy()) #Put new line into 3rd mixing bowl.
	s[2].append(new_line.copy()) #Put new line into 2nd mixing bowl.
	s[2].append(raspberry.copy()) #Put raspberry into 2nd mixing bowl.
	s[2].append(eggplant.copy()) #Put eggplant into 2nd mixing bowl.
	s[2].append(eggplant.copy()) #Put eggplant into 2nd mixing bowl.
	s[2].append(broccoli.copy()) #Put broccoli into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	s[2].append(feijoa.copy()) #Put feijoa into 2nd mixing bowl.
	s[2].append(onion.copy()) #Put onion into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	s[2].append(squach.copy()) #Put squach into 2nd mixing bowl.
	s[2].append(eggplant.copy()) #Put eggplant into 2nd mixing bowl.
	s[2].append(lime.copy()) #Put lime into 2nd mixing bowl.
	s[2].append(turnip.copy()) #Put turnip into 2nd mixing bowl.
	s[2].append(turnip.copy()) #Put turnip into 2nd mixing bowl.
	s[2].append(onion.copy()) #Put onion into 2nd mixing bowl.
	s[2].append(broccoli.copy()) #Put broccoli into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	s[2].append(eggplant.copy()) #Put eggplant into 2nd mixing bowl.
	s[2].append(raspberry.copy()) #Put raspberry into 2nd mixing bowl.
	s[2].append(onion.copy()) #Put onion into 2nd mixing bowl.
	s[2].append(mushrooms.copy()) #Put mushrooms into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	s[2].append(onion.copy()) #Put onion into 2nd mixing bowl.
	s[2].append(nectarine.copy()) #Put nectarine into 2nd mixing bowl.
	map(lambda x: x.liquefy(), s[2]) #Liquefy contents of the 2nd mixing bowl.
	d[1].extend([x.copy() for x in s[2]]) #Pour contents of the 2nd mixing bowl into the baking dish.
	d[1].extend([x.copy() for x in s[3]]) #Pour contents of the 3rd mixing bowl into the baking dish.
	printdata(d,1)
	return s[1] #Refrigerate for 1 hour.
	return s[1]
	


def take_one_down_and_pass_it_around(data=(defaultdict(list),defaultdict(list))) :
	s,d=copydata(data)
	chestnut=Ing('chestnut',105,True)
	new_line=Ing('new_line',10,False)
	watercress=Ing('watercress',119,None)
	kale=Ing('kale',107,True)
	onion=Ing('onion',111,True)
	dandelion=Ing('dandelion',100,None)
	asparagus=Ing('asparagus',97,True)
	nannyberry=Ing('nannyberry',110,True)
	tomatoe=Ing('tomatoe',84,None)
	cucumber=Ing('cucumber',117,True)
	eggplant=Ing('eggplant',101,True)
	turnip=Ing('turnip',116,True)
	pickles=Ing('pickles',32,None)
	raspberry=Ing('raspberry',114,True)
	squach=Ing('squach',115,True)
	pumpkin=Ing('pumpkin',112,True)
	ingredients=[chestnut, new_line, watercress, kale, onion, dandelion, asparagus, nannyberry, tomatoe, cucumber, eggplant, turnip, pickles, raspberry, squach, pumpkin]

	s[3].append(new_line.copy()) #Put new line into 3rd mixing bowl.
	s[2].append(dandelion.copy()) #Put dandelion into 2nd mixing bowl.
	s[2].append(nannyberry.copy()) #Put nannyberry into 2nd mixing bowl.
	s[2].append(cucumber.copy()) #Put cucumber into 2nd mixing bowl.
	s[2].append(onion.copy()) #Put onion into 2nd mixing bowl.
	s[2].append(raspberry.copy()) #Put raspberry into 2nd mixing bowl.
	s[2].append(asparagus.copy()) #Put asparagus into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	s[2].append(turnip.copy()) #Put turnip into 2nd mixing bowl.
	s[2].append(chestnut.copy()) #Put chestnut into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	s[2].append(squach.copy()) #Put squach into 2nd mixing bowl.
	s[2].append(squach.copy()) #Put squach into 2nd mixing bowl.
	s[2].append(asparagus.copy()) #Put asparagus into 2nd mixing bowl.
	s[2].append(pumpkin.copy()) #Put pumpkin into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	s[2].append(dandelion.copy()) #Put dandelion into 2nd mixing bowl.
	s[2].append(nannyberry.copy()) #Put nannyberry into 2nd mixing bowl.
	s[2].append(asparagus.copy()) #Put asparagus into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	s[2].append(nannyberry.copy()) #Put nannyberry into 2nd mixing bowl.
	s[2].append(watercress.copy()) #Put watercress into 2nd mixing bowl.
	s[2].append(onion.copy()) #Put onion into 2nd mixing bowl.
	s[2].append(dandelion.copy()) #Put dandelion into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	s[2].append(eggplant.copy()) #Put eggplant into 2nd mixing bowl.
	s[2].append(nannyberry.copy()) #Put nannyberry into 2nd mixing bowl.
	s[2].append(onion.copy()) #Put onion into 2nd mixing bowl.
	s[2].append(pickles.copy()) #Put pickles into 2nd mixing bowl.
	s[2].append(eggplant.copy()) #Put eggplant into 2nd mixing bowl.
	s[2].append(kale.copy()) #Put kale into 2nd mixing bowl.
	s[2].append(asparagus.copy()) #Put asparagus into 2nd mixing bowl.
	s[2].append(tomatoe.copy()) #Put tomatoe into 2nd mixing bowl.
	map(lambda x: x.liquefy(), s[2]) #Liquefy contents of the 2nd mixing bowl.
	d[1].extend([x.copy() for x in s[2]]) #Pour contents of the 2nd mixing bowl into the baking dish.
	d[1].extend([x.copy() for x in s[3]]) #Pour contents of the 3rd mixing bowl into the baking dish.
	printdata(d,1)
	return s[1] #Refrigerate for 1 hour.
	return s[1]
	



_99_bottles_of_beer()
