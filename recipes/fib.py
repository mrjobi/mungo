from collections import defaultdict
from random import shuffle

class Ing :
	def __init__(self,name,value,state) :
		self.name=name
		self.value=value
		self.state=state
	def __add__(self,other):
		return Ing(self.name,self.value+other.value,self.state)
	def __sub__(self,other):
		return Ing(self.name,self.value-other.value,self.state)
	def __mul__(self,other):
		return Ing(self.name,self.value*other.value,self.state)
	def __div__(self,other):
		return Ing(self.name,self.value/other.value,self.state)
	def __isub__(self,val) :
		return Ing(self.name,self.value-val,self.state)
	def __nonzero__(self) :
		return self.value!=0
	def __str__(self) :
		v=self.value
		return "%c"%v if self.state==False else " %d"%v
	def __repr__(self) :
		return str(self)
	def liquefy(self) :
		self.state=False
	def copy(self) :
		return Ing(self.name,self.value,self.state)

def stir(stack,num) :
	n=int(num)
	depth=n if len(stack)>=n else len(stack)
	i=stack.pop()
	stack.insert(-depth,i)

def copydata(data) :
	bowls,dishes=data
	
	s=defaultdict(list)
	for k,v in bowls.items() :
		s[k]=[x.copy() for x in v]
	d=defaultdict(list)
	for k,v in bowls.items() :
		d[k]=[x.copy() for x in v]
	return s,d

def printdata(dishes,count) :
	result=[]
	for i in range(1,count+1) :
		d=dishes[i]
		while d :
			result.append(str(d.pop()))
	print "".join(result)

def fibonacci_numbers(data=(defaultdict(list),defaultdict(list))) :
	s,d=copydata(data)
	fib1=Ing('fib1',0,True)
	second_iterator=Ing('second_iterator',16,True)
	iterator=Ing('iterator',16,True)
	fib2=Ing('fib2',1,True)
	ingredients=[fib1, second_iterator, iterator, fib2]

	while iterator :
		cannot_make_totally_empty_loop_in_python='noop' #Chop iterator.
		s[1].append(fib2.copy()) #Put fib2 into 1st mixing bowl.
		s[1].append(s[1][-1]+fib1) #Add fib1 into 1st mixing bowl.
		fib2=s[1].pop() #Fold fib2 into 1st mixing bowl.
		fib1=s[1].pop() #Fold fib1 into 1st mixing bowl.
		s[1].append(fib1.copy()) #Put fib1 into 1st mixing bowl.
		iterator-=1 #Chop iterator until choped.
	while second_iterator :
		cannot_make_totally_empty_loop_in_python='noop' #Mash second iterator.
		fib1=s[1].pop() #Fold fib1 into 1st mixing bowl.
		s[2].append(fib1.copy()) #Put fib1 into 2nd mixing bowl.
		second_iterator-=1 #Mash second iterator until mashed.
	d[1].extend([x.copy() for x in s[2]]) #Pour contents of 2nd mixing bowl into the baking dish.
	printdata(d,1) #Serves 1
	return s[1]
	



fibonacci_numbers()