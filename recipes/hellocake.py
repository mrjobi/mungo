from collections import defaultdict
from random import shuffle

class Ing :
	def __init__(self,name,value,state) :
		self.name=name
		self.value=value
		self.state=state
	def __add__(self,other):
		return Ing(self.name,self.value+other.value,self.state)
	def __sub__(self,other):
		return Ing(self.name,self.value-other.value,self.state)
	def __mul__(self,other):
		return Ing(self.name,self.value*other.value,self.state)
	def __div__(self,other):
		return Ing(self.name,self.value/other.value,self.state)
	def __isub__(self,val) :
		return Ing(self.name,self.value-val,self.state)
	def __nonzero__(self) :
		return self.value!=0
	def __str__(self) :
		v=self.value
		return "%c"%v if self.state==False else " %d"%v
	def __repr__(self) :
		return str(self)
	def liquefy(self) :
		self.state=False
	def copy(self) :
		return Ing(self.name,self.value,self.state)

def stir(stack,num) :
	n=int(num)
	depth=n if len(stack)>=n else len(stack)
	i=stack.pop()
	stack.insert(-depth,i)

def copydata(data) :
	bowls,dishes=data
	
	s=defaultdict(list)
	for k,v in bowls.items() :
		s[k]=[x.copy() for x in v]
	d=defaultdict(list)
	for k,v in bowls.items() :
		d[k]=[x.copy() for x in v]
	return s,d

def printdata(dishes,count) :
	result=[]
	for i in range(1,count+1) :
		d=dishes[i]
		while d :
			result.append(str(d.pop()))
	print "".join(result)

def chocolate_sauce(data=(defaultdict(list),defaultdict(list))) :
	s,d=copydata(data)
	sugar=Ing('sugar',111,True)
	water=Ing('water',1,True)
	heated_double_cream=Ing('heated_double_cream',108,False)
	milk_chocolate=Ing('milk_chocolate',72,True)
	dark_chocolate=Ing('dark_chocolate',101,True)
	hot_water=Ing('hot_water',108,False)
	ingredients=[sugar, water, heated_double_cream, milk_chocolate, dark_chocolate, hot_water]

	s[1]=[] #Clean the mixing bowl.
	s[1].append(sugar.copy()) #Put sugar into the mixing bowl.
	s[1].append(hot_water.copy()) #Put hot water into the mixing bowl.
	s[1].append(heated_double_cream.copy()) #Put heated double cream into the mixing bowl.
	while water :
		cannot_make_totally_empty_loop_in_python='noop' #Dissolve the water.
		water-=1 #Agitate the water until dissolved.
	dark_chocolate.liquefy() #Liquefy the dark chocolate.
	s[1].append(dark_chocolate.copy()) #Put dark chocolate into the mixing bowl.
	milk_chocolate.liquefy() #Liquefy the milk chocolate.
	s[1].append(milk_chocolate.copy()) #Put milk chocolate into the mixing bowl.
	map(lambda x: x.liquefy(), s[1]) #Liquefy contents of the mixing bowl.
	d[1].extend([x.copy() for x in s[1]]) #Pour contents of the mixing bowl into the baking dish.
	printdata(d,1)
	return s[1] #Refrigerate for 1 hour.
	return s[1]
	


def hello_world_cake_with_chocolate_sauce(data=(defaultdict(list),defaultdict(list))) :
	s,d=copydata(data)
	chocolate_chips=Ing('chocolate_chips',33,True)
	butter=Ing('butter',100,True)
	cocoa_powder=Ing('cocoa_powder',32,True)
	flour=Ing('flour',119,True)
	cake_mixture=Ing('cake_mixture',0,True)
	sugar=Ing('sugar',114,True)
	baking_powder=Ing('baking_powder',2,True)
	double_cream=Ing('double_cream',54,False)
	beaten_eggs=Ing('beaten_eggs',111,False)
	ingredients=[chocolate_chips, butter, cocoa_powder, flour, cake_mixture, sugar, baking_powder, double_cream, beaten_eggs]

	s[1].append(chocolate_chips.copy()) #Put chocolate chips into the mixing bowl.
	s[1].append(butter.copy()) #Put butter into the mixing bowl.
	s[1].append(sugar.copy()) #Put sugar into the mixing bowl.
	s[1].append(beaten_eggs.copy()) #Put beaten eggs into the mixing bowl.
	s[1].append(flour.copy()) #Put flour into the mixing bowl.
	s[1].append(baking_powder.copy()) #Put baking powder into the mixing bowl.
	s[1].append(cocoa_powder.copy()) #Put cocoa powder into the mixing bowl.
	stir(s[1],1) #Stir the mixing bowl for 1 minute.
	s[1].append(s[1][-1]*double_cream) #Combine double cream into the mixing bowl.
	stir(s[1],5) #Stir the mixing bowl for 5 minutes.
	map(lambda x: x.liquefy(), s[1]) #Liquefy the contents of the mixing bowl.
	chocolate_chips=s[1].pop() #Fold chocolate chips into mixing bowl.
	d[1].extend([x.copy() for x in s[1]]) #Pour contents of the mixing bowl into the baking dish.
	while cake_mixture :
		cannot_make_totally_empty_loop_in_python='noop' #Bake the cake mixture. #Wait until baked.
	s[1].extend(chocolate_sauce((s,d))) #Serve with chocolate sauce.
	while baking_powder :
		cannot_make_totally_empty_loop_in_python='noop' #Ignore the baking powder.
		s[2].append(chocolate_chips.copy()) #Put chocolate chips into 2nd mixing bowl.
		baking_powder-=1 #Ignore the baking powder until ignored.
	return s[1] #Refrigerate.
	d[2].extend([x.copy() for x in s[2]]) #Pour contents of the 2nd mixing bowl into the 2nd baking dish.
	printdata(d,2) #Serves 2
	return s[1]
	



hello_world_cake_with_chocolate_sauce()