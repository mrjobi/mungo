class Config :
	DEBUG_STOP="stop"
	DEBUG_PRINT="print"
	DEBUG_PRINT_DS="print-ds"
	DEBUG_NONE=None
	
	dflag=DEBUG_NONE
	spaces_from_nums=True #putting space between numbers is not in language spec, but otherwise the output is unreadable
