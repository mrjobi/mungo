from collections import defaultdict
from random import shuffle

class Dish :
	"A Bowl is a stack of ingredient amounts, can be a MixingBowl or a BakingDish"
	def __init__(self) :
		self.ingredients = []
		self.name="some dish"
		
	def __repr__(self) :
		return "(%s) : %s"%(self.name,self.ingredients)
			
	def copy(self) :
		return [x.copy() for x in self.ingredients]

class Bowl(Dish) :
	"A Bowl is a stack of ingredient amounts, can be a MixingBowl or a BakingDish"
	def __init__(self) :
		self.ingredients = []
		self.name="some bowl"

	def stir(self, num) :
		n=int(num)
		depth=n if len(self.ingredients)>=n else len(self.ingredients)
		ing=self.ingredients.pop()
		self.ingredients.insert(-depth,ing)

	def mix(self) :
		shuffle(self.ingredients)
		
	def clean(self) :
		self.ingredients = []
	
	def append(self, ing) :
		self.ingredients.append(ing)
	
	def pop(self) :
		if self.ingredients : return self.ingredients.pop()
		raise ValueError("Bowl is empty, cannot fold ingredient")
		
	def top(self) :
		if (self.ingredients) : return self.ingredients[-1]
	
	def newtop(self, val) :
		ing=self.top().copy()
		ing.value=val
		self.append(ing)
			
	def pour_to(self, dish) :
		dish.ingredients.extend(self.ingredients)
		
	def liquefy(self) :
		for i in self.ingredients : i.state=False

class Dataset :	
	def __init__(self, ingredients) :
		self.ingredients=ingredients
		self.mixingBowls=defaultdict(Bowl)
		self.bakingDishes=defaultdict(Dish)
		
	def bowl(self, num) :
		n=int(num) if num else 1
		bowl=self.mixingBowls[n]
		bowl.name="bowl %s"%n
		return bowl
		
	def dish(self, num) :
		n=int(num) if num else 1
		dish=self.bakingDishes[n]
		dish.name="baking %s"%n
		return dish
		
	def ing(self, n) :
		if n in self.ingredients.keys():
			return self.ingredients[n]
		raise ValueError("Ingredient %s is not available"%n)
		
	def topbowl(self, num) :
		r=self.bowl(num).top()
		if not r : raise ValueError("Mixing bowl %s is empty, cannot get ingredient"%num)
		return r.value
		
	def newtop(self, num, val) :
		bowl=self.bowl(num).newtop(val)
		
	def copy(self) :
		bowls=defaultdict(Bowl)
		for k,v in self.mixingBowls.items() :
			bowls[k].ingredients=v.copy()
		dishes=defaultdict(Dish)
		for k,v in self.bakingDishes.items() :
			dishes[k].ingredients=v.copy()
		return (bowls,dishes)
		
	def __repr__(self) :
		bowls=["%s"%v for v in self.mixingBowls.values()]
		dishes=["%s"%v for v in self.bakingDishes.values()]
		return "ingredients %s bowls %s dishes %s"%(self.ingredients,bowls,dishes)
