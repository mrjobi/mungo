import sys

from parser import *
from dataset import *
from operations import operations
from config import Config

ops=operations()
num_print=" %d" if Config.spaces_from_nums else "%d"

def debug(string, ds=None) :
	if Config.dflag :
		print string
	if Config.dflag==Config.DEBUG_PRINT_DS :
		print ds
	if Config.dflag==Config.DEBUG_STOP :
		cmd=raw_input()	
		if cmd=="ds" :
			print ds, "\n", string
		elif cmd=="i" :
			print ds.ingredients, "\n", string
		elif cmd=="b" :
			print ds.mixingBowls, "\n", string
		elif cmd=="d" :
			print ds.bakingDishes, "\n", string
		else :
			pass
		
class Loop :
	def __init__(self, instruction) :
		verb, self.ingredient=instruction
		self.verb=verb.lower()
		self.operations=[]
	def check(self, ds) :
		return ds.ingredients[self.ingredient].value!=0
	def op(self, op) :
		self.operations.append(op)
	def set_aside(self) :
		self.operations.append(None)
	def finish(self, ingredient=None) :
		self.end_ingredient=ingredient
	def __repr__(self) :
		return "%s %s -- %s"%(self.verb,self.ingredient,self.operations)
	def exe(self, ds) :
		debug("--execute loop: %s %s"%(self.verb,self.ingredient),ds)
		
		while self.check(ds) :
			debug("---loop %s=%s"%(self.ingredient, ds.ingredients[self.ingredient].value),ds)
			for i in self.operations :
				if not i:
					debug("----set aside.")
					return
				debug("----%s"%i,ds)
				res=i.exe(ds)
				if res : return res
			if self.end_ingredient :
				ds.ingredients[self.end_ingredient].value-=1
				debug("---%s %s until %sed"%(self.verb,self.end_ingredient if self.end_ingredient else "",self.verb),ds)
		debug("--loop done.")
		
class Step :
	def __init__(self, i) :
		self.instr=i
		self.op=i.name
		self.args=i.args
	def __repr__(self) : 
		return "%s. %s -- %s %s"%(self.instr.line,self.instr.text,self.instr.name,self.instr.args)
	def exe(self, ds) :
		try :
			ops[self.instr.name](ds,self.instr.args)
		except ValueError as v :
			raise RecipeError(self.instr.line, v.message)

class SousChef :
	def __init__(self, script) :
		self.script=script
	def __repr__(self) :
		return "Sous-chef: %s"%self.script[0]
	def exe(self, ds) :
		debug(">>> execute sous-chef: %s"%self.script[0].capitalize())
		sous_chef=Executor()
		result=sous_chef.exe(self.script, ds.copy())
		result.pour_to(ds.bowl(1))
		
class Refrigerate :
	def __init__(self, serve, num) :
		self.num=num
		self.serve=serve
	def __repr__(self) :
		return "Refrigerate %s"%(self.num if self.num else "")
	def exe(self, ds) :
		if self.num :
			self.serve(int(self.num),ds)
		return ds.bowl(1)
					
class Executor :
	def __init__(self, chefs=None) :
		self.loops=[]
		self.chefs=chefs or {}
		return
		
	def serve(self,num,ds) :
		bd=ds.bakingDishes
		chars=[]
		for i in range(1,num+1) :
			dish=bd[i]
			ingredients=dish.ingredients if dish else None
			while ingredients :
				ch=ingredients.pop()
				chars.append(chr(ch.value) if ch.state==False else num_print%ch.value)
		print "".join(chars)
		
	def findStep(self,i) :
		op=i.name
		if op in ops :
			return Step(i)
		elif op=='verb' :
			l=Loop(i.args)
			self.loops.append(l)
		elif op=='set_aside' :
			if self.loops :
				self.loops[-1].set_aside()
			else :
				raise RecipeError(i.line, "Set aside appearing outside of Verb block")
		elif op=='until_verbed' :
			verb, verbed=self.loops[-1].verb,i.args[1]
			verb=verb.rstrip("e")
			if self.loops and verb==verbed :
				loop=self.loops.pop()
				loop.finish(i.args[0])
				return loop #ended loop is an op
			else :
				raise RecipeError(i.line, "<%s> block end does not match the <%sd>"%(verb,verbed)) 
		elif op=='serve_with' :
			recipe=i.args[0].lower()
			if recipe not in self.chefs :
				self.chefs[recipe]=SousChef((recipe,self.recipes))
			return self.chefs[recipe] 
		elif op=='refrigerate' :
			return Refrigerate(self.serve, i.args[0])
					
	def exe(self, script, data=None) :
		name,self.recipes=script
		recipe=self.recipes[name]
		
		ds=Dataset(recipe.ingredients)
		steps=[]
		debug("executing %s, %s"%(name.capitalize(),"inherited %s, %s"%data if data else "no data"))
		if data :
			ds.mixingBowls,ds.bakingDishes=data
		
		for i in recipe.instructions :
			step=self.findStep(i)
			if step :
				if (self.loops) :
					self.loops[-1].op(step)
				else :
					steps.append(step)
		
		for step in steps :
			debug("%s"%step,ds)
			res=step.exe(ds)
			if res : 
				return ds.bowl(1)
		
		if recipe.serves :
			self.serve(recipe.serves, ds)
	
		return ds.bowl(1)
