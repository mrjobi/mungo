from executor import *
from operations import operations
ops=operations()


test_dataset=Dataset({
	"eggs":Ingredient(12,"eggs",None),
	"carrots":Ingredient(20,"carrots",True),
	"milk":Ingredient(25,"milk",False),
	"tomato":Ingredient(2,"tomato",True),
	"sugar":Ingredient(1,"sugar",True),
	})



def assert_is(name, value, state) :
	assert test_dataset.ingredients[name].value==value
	assert test_dataset.ingredients[name].state==state

def assert_i(name, value) :
	print test_dataset.ingredients[name].value, value
	assert test_dataset.ingredients[name].value==value

def assert_b(val,n=1) :
	print test_dataset.bowl(n), val
	assert [i.value for i in test_dataset.bowl(n).ingredients]==val
	
def assert_p(d,b) :
	dishvals=[x.value for x in test_dataset.dish(d).ingredients]
	bowlvals=[x.value for x in test_dataset.bowl(b).ingredients]
	print bowlvals, dishvals
	assert dishvals==bowlvals
	
def set_ing(val,n=1) :
	test_dataset.bowl(n).ingredients = [Ingredient(x,"...",None) for x in val]

def mcall(name, *args) :
	print args
	ops[name](test_dataset, [args[0],args[1] if len(args)>1 else None])

#"take" has been tested, not possible to auto test because requires input
#mcall("take","eggs")
#print test_dataset.ingredients

mcall("put","eggs")
mcall("put","carrots",1)
print test_dataset
assert_b([12,20])

mcall("fold","milk",1)
assert_b([12])
assert_i("milk",20)

mcall("add","carrots",1)
assert_b([12,32])

mcall("remove","eggs",1)
assert_b([12,32,20])

print ">>>dish--",test_dataset.dish(80).ingredients

mcall("combine","tomato")
assert_b([12,32,20,40])

mcall("divide","carrots")
assert_b([12,32,20,40,2])

mcall("liquefy_ing","eggs")
assert_is("eggs",12,False)

set_ing([1,2,3,4,5],3)
mcall("stir_time",3,1)
assert_b([1,2,3,5,4],3)

set_ing([1,2,3,4,5],3)
mcall("stir_time",3,4)
assert_b([5,1,2,3,4],3)

set_ing([1,2,3,4,5],3)
mcall("stir_time",3,7)
assert_b([5,1,2,3,4],3)

set_ing([1,2,3,4,5],3)
test_dataset.ingredients["sugar"].value=1
mcall("stir_ing","sugar",3)
assert_b([1,2,3,5,4],3)

set_ing([1,2,3,4,5],3)
test_dataset.ingredients["sugar"].value=4
mcall("stir_ing","sugar",3)
assert_b([5,1,2,3,4],3)

set_ing([1,2,3,4,5],3)
test_dataset.ingredients["sugar"].value=7
mcall("stir_ing","sugar",3)
assert_b([5,1,2,3,4],3)

set_ing([1,2,3,4,5],3)
mcall("mix",3)
print test_dataset.mixingBowls[3].ingredients


set_ing([1,2,3,4,5],3)
mcall("clean",3)
assert_b([],3)

print ">>>dish--",test_dataset.bakingDishes[80].ingredients
set_ing([1,2,3,4,5],11)
mcall("pour",11,2)
assert_p(2,11)
