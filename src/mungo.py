#!/usr/bin/python
"""
Mungo.
Chef interpreter written in python.
Chef is an esoteric programming language by David Morgan-Mar - http://www.dangermouse.net/esoteric/chef.html

Mungo is the name of the Cook in Monty Python's "Restaurant Sketch" (a.k.a Dirty Fork)
(c) 2014 marek.job, licensed under WTFPL http://www.wtfpl.net/
"""
import sys
from parser import Parser, RecipeError
from executor import Executor
from config import Config

Config.dflag=Config.DEBUG_NONE
parser=Parser()
executor=Executor()

def execute(name) :
	lines=open("%s.chef"%name).readlines()
	try :
		recipes=parser.readRecipes(lines)
		executor.exe(recipes)
	except RecipeError as err :
		print err

if __name__=="__main__" :
	arg=sys.argv[1]
	execute(arg.replace(".chef",""))
