from parser import Ingredient

def operations() :
	ops={}
	def take(ds, a) :
		ds.ingredients[a[0]].value=int(raw_input())
	ops['take']=take
	def put(ds, a) :
		bowl=ds.bowl(a[1])
		bowl.append(ds.ingredients[a[0]].copy())
	ops['put']=put
	def fold(ds, a) :
		bowl=ds.bowl(a[1])
		ds.ing(a[0]).value=bowl.pop().value
	ops['fold']=fold
	def add(ds, a) :
		b=a[1]		
		ds.newtop(b,ds.topbowl(b)+ds.ing(a[0]).value)		
	ops['add']=add
	def remove(ds, a) :
		b=a[1]		
		ds.newtop(b,ds.topbowl(b)-ds.ing(a[0]).value)	
	ops['remove']=remove
	def combine(ds, a) :
		b=a[1]		
		ds.newtop(b,ds.topbowl(b)*ds.ing(a[0]).value)	
	ops['combine']=combine
	def divide(ds, a) :
		b=a[1]		
		ds.newtop(b,ds.topbowl(b)/ds.ing(a[0]).value)
	ops['divide']=divide
	def add_dry(ds, a) :
		bowl=ds.bowl(a[0])
		val=sum([x.value for x in ds.ingredients.values() if x.state])
		bowl.append(Ingredient(val,"sum",True))
	ops['add_dry']=add_dry
	def liquefy_ing(ds, a) :
		ds.ing(a[0]).state=False
	ops['liquefy_ing']=liquefy_ing
	def liquefy_bowl(ds, a) :
		ds.bowl(a[1]).liquefy()
	ops['liquefy_bowl']=liquefy_bowl
	def stir_time(ds, a) :
		ds.bowl(a[0]).stir(a[1])
	ops['stir_time']=stir_time
	def stir_ing(ds, a) :
		val=ds.ingredients[a[0]].value
		ds.bowl(a[1]).stir(val)
	ops['stir_ing']=stir_ing
	def mix(ds, a) :
		ds.bowl(a[0]).mix()
	ops['mix']=mix
	def clean(ds, a) :
		ds.bowl(a[0]).clean()
	ops['clean']=clean
	def pour(ds, a) :
		ds.bowl(a[0]).pour_to(ds.dish(a[1]))
	ops['pour']=pour
	
	
	return ops
