import re

class Ingredient :
	"An ingredient holds a numeric value if state or a unicode character if liquid"
	def __init__(self, value, name, state) :
		self.value=value
		self.name=name
		self.state=state
		#print re.compile("[ ]*([\-\w][\- \w]*)[ ]*$").match(name).groups()
	def __repr__(self) :
		return "%c"%self.value if self.state==False else "%d"%self.value
	def copy(self) :
		return Ingredient(self.value, self.name, self.state)
		
class Instruction :
	"An instruction holds a step to execute"
	def __init__(self, lineno, instruction, args, text) :
		self.line=lineno
		self.name=instruction[:-2] if instruction.endswith("_b") else instruction
		self.args=[args[0],args[1] if len(args)>1 else None]
		self.text=text
	def __repr__(self) :
		return "%s %s %s -- %s" % (self.line, self.name, self.args, self.text)

class Recipe :
	"A recipe"
	ingredients={}
	instructions=[]
	def __init__(self, title) :
		self.title=title
	def __repr__(self) :
		return """\ntitle: %s
ingredients: %s 
instructions: %s
serves: %s
""" % (self.title, self.ingredients, self.instructions, self.serves)
		
class RecipeError(Exception) :
	def __init__(self, line, message) :
		self.line=line
		self.msg=message
	def __str__(self) :
		return "RecipeError at line %d: %s" % (self.line,self.msg)
		
def blank(blank) :
	return len(blank.strip())==0

class IngredientParser :
	#todo take care about the amount being optional
	dry=[re.compile("(?P<amount>\d+)(?: (?:heaped|level) (?:cup[s]?|teaspoon[s]?|tablespoon[s]?)) (?P<name>.*)"), re.compile("(?P<amount>\d+) (g|kg|pinch(?:es)?) (?P<name>.*)")]
	unspec=re.compile("(?P<amount>\d+)(?: (?:cup[s]?|teaspoon[s]?|tablespoon[s]?))? (?P<name>.*)")
	liquids=re.compile("(?P<amount>\d+) (ml|l|dash(?:es)?) (?P<name>.*)")
	
	def parse(self, lineno, ingredient) :
		match=self.liquids.match(ingredient)
		if match :
			return Ingredient(int(match.group('amount')), match.group('name'), False)
		for reg in self.dry :
			match = reg.match(ingredient)
			if match :
				return Ingredient(int(match.group('amount')), match.group('name'), True)
		match = self.unspec.match(ingredient)
		if match :
			return Ingredient(int(match.group('amount')), match.group('name'), None)
		raise RecipeError(lineno, "Invalid ingredient specification <%s>"%ingredient)

class InstructionParser :
	i="(?:the )?(?P<i>.*)"
	b="(?:the )?(?:(?P<b>\d+)(?:st|nd|rd|th) )?mixing bowl"
	d="(?:the )?(?:(?P<d>\d+)(?:st|nd|rd|th) )?baking dish"
	n="(?P<n>\d+)"
	expr={
	'take':re.compile("Take {} from refrigerator\.".format(i)),
	'put':re.compile("Put {} into {}\.".format(i,b)),
	'fold':re.compile("Fold {} into {}\.".format(i,b)),
	'add_b':re.compile("Add {} (?:in)?to {}\.".format(i,b)),
	'add':re.compile("Add {}\.".format(i)),
	'remove_b':re.compile("Remove {} from {}\.".format(i,b)),
	'remove':re.compile("Remove {}\.".format(i)),
	'combine_b':re.compile("Combine {} into {}\.".format(i,b)),
	'combine':re.compile("Combine {}\.".format(i)),
	'divide':re.compile("Divide {}\.".format(i)),
	'divide_b':re.compile("Divide {} into {}\.".format(i,b)),
	'add_dry_b':re.compile("Add dry ingredients (?:in)?to {}\.".format(b)),
	'add_dry':re.compile("Add dry ingredients()?\."),
	'liquefy_bowl':re.compile("Liquefy(?: the)? contents of the {}\.".format(b)),
	'liquefy_ing':re.compile("Liquefy {}\.".format(i)),
	'stir_time':re.compile("Stir(?: {})? for {} minute[s]?\.".format(b,n)),
	'stir_ing':re.compile("Stir {} into the {}\.".format(i,b)),
	'mix':re.compile("Mix(?: {})? well\.".format(b)),
	'clean':re.compile("Clean {}\.".format(b)),
	'pour':re.compile("Pour contents of {} into {}\.".format(b,d)),
	'set_aside':re.compile("Set aside()?\."),
	'refrigerate':re.compile("Refrigerate(?: for {} hour[s]?)?\.".format(n)),
	'serve_with':re.compile("Serve with {}\.".format(i)),
	'verb':re.compile("(?P<v>[\-\w]+) {}\.".format(i)),
	'until_verbed':re.compile("(?:[\-\w]+)(?: {})? until (?P<v>[\-\w]+)ed".format(i)),
	}

	order=['take','put','fold','add_dry_b','add_dry','add_b','add','remove_b','remove','combine_b','combine', 
		'divide_b','divide','liquefy_bowl','liquefy_ing','stir_time','stir_ing','mix','clean','pour','set_aside',
		'refrigerate','serve_with','until_verbed','verb']
	
	def parse(self, lineno, instruction) :
		for i in self.order :
			match=self.expr[i].match(instruction)
			if match :
				return Instruction(lineno, i, match.groups(), instruction)
		raise RecipeError(lineno, "Invalid method step <%s>"%instruction)
		
		
class Parser :
	ingredientParser=IngredientParser()
	instructionParser=InstructionParser()
	lineno=0;
	
	def section(self, lines) :
		if not lines :
			return False
		if blank(lines[0]) :
			self.nextline(lines)
			return False
		return True
	
	def nextline(self, lines) :
		self.lineno+=1
		return lines.pop(0)
	
	def readTitle(self, lines) :
		p=re.compile("^[ ]*([\-\w][\- \w]*)\.[ ]*$")
		line=self.nextline(lines)
		section=self.section(lines)
		match=p.match(line)

		if match and not section :
			return match.group(1)
		else :
			raise RecipeError(self.lineno, "Recipe must start with a title, <%s> doesn't seem to be a valid title"%line)
	
	def handleComments(self, lines) :
		while self.section(lines) :
			if lines[0]=="Ingredients." :
				#there wero no comments, ingredients are taken care of in next step
				return
			self.nextline(lines)
		
	def handleIngredient(self, line) :
		return self.ingredientParser.parse(self.lineno, line)
			
	def readIngredients(self, lines) :
		ingredients={}
		header=self.nextline(lines)
		if header!="Ingredients." :
			lines.insert(0, header)
			return ingredients
		
		while self.section(lines) :
			ing=self.handleIngredient(self.nextline(lines))
			ingredients[ing.name]=ing

		return ingredients
		
	def handleOptional(self, lines) :
		 #for "Cooking time" and "Pre-heat oven" - doesn't actually matter for recipe execution
		time=re.compile("^[ ]*Cooking time:[ ]*(\d+)(?: hours?| minutes?)\.[ ]*$")
		temp=re.compile("^[ ]*Pre-heat oven to (\d+) degrees Celsius(?: \(gas mark (\d+)\))?\.[ ]*$")

		if time.match(lines[0]) : 
			self.nextline(lines)
			self.nextline(lines)
		if temp.match(lines[0]) :
			self.nextline(lines)
			self.nextline(lines)
		return
				
	def readMethod(self, lines) :
		instructions = []
		header=self.nextline(lines)
		if header!="Method." :
			raise RecipeError(self.lineno, "Expected Method section and found <%s>"%header)
			
		while self.section(lines) :
			line=self.nextline(lines)
			steps=re.findall('[^.]+\.',line)
			for step in steps :
				if step.startswith("Liquify") :
					print "Warning at line %s: Liquify is deprecated, use Liquefy. <%s>"%(self.lineno,step.strip())
					step=step.replace("Liquify","Liquefy")
				instructions.append(self.instructionParser.parse(self.lineno, step.strip()))
		
		return instructions
	
	def readServes(self, lines) :
		if not self.section(lines) :
			return
			
		header=self.nextline(lines)
		match=re.compile("Serves (?P<diners>\d+)\.").match(header)

		if not match :
			lines.insert(0,header)
			return
		
		diners=match.group('diners')
		self.section(lines)
		return int(diners)
		
	def skipBlanklines(self, lines) :
		while lines and blank(lines[0]) :
			self.nextline(lines)
				
	def readRecipes(self, lines) :		
		lines=map(lambda s: s.rstrip("\r\n"),lines)
		recipes={}
		main=None
		while lines :
			title=self.readTitle(lines).lower()
			recipe=Recipe(title)
			self.handleComments(lines)
			recipe.ingredients=self.readIngredients(lines)
			self.handleOptional(lines)
			recipe.instructions=self.readMethod(lines)
			recipe.serves=self.readServes(lines)
			self.skipBlanklines(lines)
			recipes[title]=recipe
			if not main : #main recipe is the first one in the file
				main=title

		return main, recipes
