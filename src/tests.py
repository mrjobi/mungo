import re
from parser import *

lst="""72 g hazelnuts
101 eggs
108 pinches lobsters
111 ml orange juice
44 kg cashews
32 g sugar
87 ml water
114 g rice
100 g durian
33 passion fruit
10 ml lemon juice
2 cups vodka
12 dashes cider
1 tablespoon wine
1 heaped cup cinnamon mixed with vinegar
2 heaped cups ginger
1 level tablespoons pepper
2 level teaspoons salt
1 cup white sugar"""

total=0
found=0
ip=IngredientParser()
for ing in lst.split("\n") :
	total+=1
	try :
		i=ip.parse(0,ing)
		print "{0}={1} ({2}) -- {3}".format(i.value, i.name, i.state,ing)
		found+=1
	except Exception as e:
		print e
		
print "\n {0} out of {1} parsed".format(found, total)
assert found==total	


"""
    g | kg | pinch[es] : These always indicate dry measures.
    ml | l | dash[es] : These always indicate liquid measures.
    cup[s] | teaspoon[s] | tablespoon[s] : These indicate measures which may be either dry or liquid. 
"""

lst="""Take lemon from refrigerator.
Put rice into mixing bowl.
Put orange juice into the mixing bowl.
Put eggs into the 3rd mixing bowl.
Put hazelnuts into 1st mixing bowl.
Fold butter into mixing bowl.
Fold butter into the 2nd mixing bowl.
Add salmon.
Add sugar to mixing bowl.
Add salt to 2nd mixing bowl.
Put flour into mixing bowl.
Serve with caramel sauce.
Stir for 2 minutes.
Remove egg.
Remove the snails from 7th mixing bowl.
Combine rice.
Combine rice into mixing bowl.
Divide rice.
Divide rice into the mixing bowl.
Divide rice into the 3rd mixing bowl.
Liquefy chicken.
Liquefy contents of the 4th mixing bowl.
Rub flour until sifted.
Stir for 2 minutes.
Stir the 2nd mixing bowl for 13 minutes.
Stir milk into the 16th mixing bowl.
Mix well.
Mix 2nd mixing bowl well.
Mix the 11122th mixing bowl well.
Clean mixing bowl.
Clean 2nd mixing bowl.
Clean the 12th mixing bowl.
Pour contents of the mixing bowl into the baking dish.
Pour contents of the 2nd mixing bowl into the 2nd baking dish.
Pour contents of 2nd mixing bowl into baking dish.
Set aside.
Refrigerate.
Refrigerate for 14 hours.
Serve with chicken.
Serve with aaaaaaasgdasogs sdijosaf sdjfisdfjgvofd.
Sift flour.
Something the something something.
Mix until heated.
Heat the chicken until boiled.
Heat chanel no 5 until expired.
Whatever until melted.
Put chocolate chips into the mixing bowl.
Put butter into the mixing bowl.
Put sugar into the mixing bowl.
Put beaten eggs into the mixing bowl.
Put flour into the mixing bowl.
Put baking powder into the mixing bowl.
Put cocoa powder into the mixing bowl.
Stir the mixing bowl for 1 minute.
Combine double cream into the mixing bowl.
Stir the mixing bowl for 4 minutes.
Liquefy the contents of the mixing bowl.
Pour contents of the mixing bowl into the baking dish.
bake the cake mixture.
Wait until baked.
Serve with chocolate sauce.
Chop iterator.
Put fib2 into 1st mixing bowl.
Put fib2 into 1st mixing bowl.
Add fib1 to 1st mixing bowl.
Fold fib2 into 1st mixing bowl.
Fold fib1 into 1st mixing bowl.
Put fib1 into 1st mixing bowl.
Chop iterator until choped.
Mash second iterator.
Fold fib1 into 1st mixing bowl.
Put fib1 into 2nd mixing bowl.
Mash second iterator until mashed.
Pour contents of 2nd mixing bowl into the baking dish.
Add dry ingredients.
Add dry ingredients to mixing bowl.
Add dry ingredients into mixing bowl.
Add dry ingredients to 1st mixing bowl."""

ip=InstructionParser()
total=0
found=0
for l in lst.split("\n") :
	total+=1
	try :
		print ip.parse(0, l)
		found+=1
	except Exception as e:
		print e
		
print "\n {0} out of {1} parsed".format(found, total)
assert found==total	
