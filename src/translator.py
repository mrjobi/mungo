from parser import *
import sys


parser=Parser()

fun={
'take':"{0}=raw_input()",
'put':"s[{1}].append({0}.copy())",
'fold':"{0}=s[{1}].pop()",
'add':"s[{1}].append(s[{1}][-1]+{0})",
'add':"s[{1}].append(s[{1}][-1]+{0})",
'remove':"s[{1}].append(s[{1}][-1]-{0})",
'remove':"s[{1}].append(s[{1}][-1]-{0})",
'combine':"s[{1}].append(s[{1}][-1]*{0})",
'combine':"s[{1}].append(s[{1}][-1]*{0})",
'divide':"s[{1}]=.append([{1}][-1]/{0})",
'divide':"s[{1}]=.append([{1}][-1]/{0})",
'stir_time':"stir(s[{0}],{1})",
'stir_ing':"stir(s[{1}],{0})",
'mix':"shuffle(s[{0}])",
'clean':"s[{0}]=[]",
'pour':"d[{1}].extend([x.copy() for x in s[{0}]])",
'set_aside':"break",
'serve_with':"s[1].extend({0}((s,d)))",
'liquefy_ing':"{0}.liquefy()",
'liquefy_bowl':"map(lambda x: x.liquefy(), s[{0}])",
'add_dry':"s[{0}].append(Ing('sum',sum([x.value for x in ingredients if x.state]),True))"
}

preamble="""from collections import defaultdict
from random import shuffle"""

stir="""def stir(stack,num) :
	n=int(num)
	depth=n if len(stack)>=n else len(stack)
	i=stack.pop()
	stack.insert(-depth,i)
"""
copydata="""def copydata(data) :
	bowls,dishes=data
	
	s=defaultdict(list)
	for k,v in bowls.items() :
		s[k]=[x.copy() for x in v]
	d=defaultdict(list)
	for k,v in bowls.items() :
		d[k]=[x.copy() for x in v]
	return s,d
"""

printdata="""def printdata(dishes,count) :
	result=[]
	for i in range(1,count+1) :
		d=dishes[i]
		while d :
			result.append(str(d.pop()))
	print "".join(result)
"""

ingredient="""
class Ing :
	def __init__(self,name,value,state) :
		self.name=name
		self.value=value
		self.state=state
	def __add__(self,other):
		return Ing(self.name,self.value+other.value,self.state)
	def __sub__(self,other):
		return Ing(self.name,self.value-other.value,self.state)
	def __mul__(self,other):
		return Ing(self.name,self.value*other.value,self.state)
	def __div__(self,other):
		return Ing(self.name,self.value/other.value,self.state)
	def __isub__(self,val) :
		return Ing(self.name,self.value-val,self.state)
	def __nonzero__(self) :
		return self.value!=0
	def __str__(self) :
		v=self.value
		return "%c"%v if self.state==False else " %d"%v
	def __repr__(self) :
		return str(self)
	def liquefy(self) :
		self.state=False
	def copy(self) :
		return Ing(self.name,self.value,self.state)
"""

common="\n".join([preamble,ingredient,stir,copydata,printdata])

def rpl(s) :
	try:
		return int(s)
	except ValueError :
			s=s.replace(" ","_")
			return "_%s"%s if s[:1].isdigit() else s
	except TypeError :
		return  1

def tr(a) :
	if len(a)==0 : return 1,1 
	if len(a)==1 : return rpl(a[0]), 1
	if len(a)==2 : return rpl(a[0]), rpl(a[1])

def ind(i) :
	return "\n"+"".join("\t" for n in range(i))

def fformat(s,a,i) :
	return ind(i)+s.format(tr(a)[0],tr(a)[1])

def translate(rec) :
	lines=[]
	lines+="\n"
	
	indent=1
	lines+="def %s(data=(defaultdict(list),defaultdict(list))) :\n"%rpl(rec.title)
	lines+="\ts,d=copydata(data)"
	inglist=[]
	for val in rec.ingredients.values() :
		iname=rpl(val.name)
		inglist.append(iname)
		lines+=ind(indent)+"{0}=Ing('{0}',{1},{2})".format(iname,val.value,val.state)
	lines+=ind(indent)+"ingredients=[%s]"%", ".join(inglist)
	lines+="\n"
	for step in rec.instructions :
		sn=step.name
		if sn in fun :
			lines+=fformat(fun[sn],step.args,indent)
		elif sn=="refrigerate" :
			count=step.args[0]
			if count :
				lines+=ind(indent)+"printdata(d,%s)"%count
			lines+=ind(indent)+"return s[1]"
		elif sn=='verb' :
			lines+=fformat("while {1} :",step.args,indent)
			indent+=1
			lines+=ind(indent)+"cannot_make_totally_empty_loop_in_python='noop'"
		elif sn=='until_verbed' :
			if (step.args[0]) :
				lines+=fformat("{0}-=1",step.args,indent)
			indent-=1
		lines+=" #%s"%step.text
				
	if rec.serves :
		lines+=ind(indent)+"printdata(d,{0}) #Serves {0}".format(rec.serves)
	lines+=ind(indent)+"return s[1]"
	
	lines+=ind(indent)+"\n\n"	
	return "".join(lines)


def execute(name) :
	lines=open("%s.chef"%name).readlines()
	output=open("%s.py"%name,"w+")
	output.write(common)
	
	try :
		recipes=parser.readRecipes(lines)
		
		for rec in recipes[1].values() :
			output.write(translate(rec))
		output.write("\n\n%s()"%rpl(recipes[0]))
	except RecipeError as err :
		print err
	output.close()

if __name__=="__main__" :
	arg=sys.argv[1]
	execute(arg.replace(".chef",""))
